#!/bin/bash

#tri des notes
echo "Veuillez saisir une note :"
read note as integer

if [ $note -ge 16 ] && [ $note -le 20 ]
then 
  echo "très bien"
elif [ $note -ge 14 ] && [ $note -le 16 ]
then 
  echo "bien"
elif [ $note -ge 12 ] && [ $note -le 14 ]
then 
  echo "assez bien"
elif [ $note -ge 10 ] && [ $note -le 12 ]
then 
  echo "moyen"
else 
  echo "insuffisant"
fi
