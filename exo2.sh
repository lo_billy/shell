#!/bin/bash

function fonction1
{
echo "veuillez entrer le nom d'utilisateur : "
read username

number=$(grep -c $username /etc/passwd)

if [ $number != 0 ]
then
echo "L'utilisateur existe"
else
echo "L'utilisateur n'existe pas."
fi
}

function fonction2
{
echo "Veuillez entrer le nom d'utilisateur : "
read username

number=$(grep ^$username /etc/passwd | cut -d":" -f3)
echo "Le GUI de $username est $number"
}

#MENU#

a=0

while [ $a != 1 ] && [ $a != 2 ] && [ $a != q ]
do 
echo -e "\n1 - Vérifier l'existence d'un User\n2 - Connaitre sun UID\nq - Quitter\n"
read a
done

if [ $a == 1 ]; then
fonction1
elif [ $a == 2 ]; then
fonction2
else
exit
fi
